# tactile-urchin

A simple Vue.js app to generate heroku-like app names!

Made using TailwindCSS, Haikunator and Vue Cli!

Deployed on [Netlify!](https://haikunator.netlify.app/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
